import React from 'react';
import { FooterComponent, HeaderComponent } from '@/components';
import { Container } from './styles';

const Layout = ({ children }) => {
  return (
    <Container>
     <HeaderComponent/>
     <main>{children}</main> 
     <FooterComponent/>
    </Container>
  );
};

export default Layout;
