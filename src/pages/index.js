import {
  AccomplishmentsComponent,
  HeroComponent,
  ProjectsComponent,
  TechnologiesComponent,
  TimeLineComponent
} from '@/components';
import Layout from '@/layout';

const Home = () => {
  return (
    <Layout>
      {/* <Section grid> */}
        <HeroComponent />
        {/* <BgAnimation /> */}
      {/* </Section> */}
      <ProjectsComponent />
      <TechnologiesComponent />
      <TimeLineComponent />
      <AccomplishmentsComponent />
    </Layout>
  );
};

export default Home;
